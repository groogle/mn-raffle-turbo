package com.puravida.input


import com.puravida.model.Raffle
import com.puravida.usecases.CommitAPrizeUseCase
import com.puravida.usecases.RaffleAPrizeUseCase
import com.puravida.usecases.ReadRaffleUseCase
import com.puravida.usecases.ValidateAccessTokenUseCase
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import io.micronaut.core.annotation.Nullable
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.micronaut.views.View
import io.micronaut.views.turbo.TurboFrameView

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Slf4j
@Controller("/")
class IndexController{

    String cookieName

    ValidateAccessTokenUseCase validateAccessTokenUseCase

    ReadRaffleUseCase readRaffleUseCase

    RaffleAPrizeUseCase raffleAPrizeUseCase

    CommitAPrizeUseCase commitAPrizeUseCase

    IndexController(@Value('${raffle.cookie}') String cookieName,
                    ValidateAccessTokenUseCase validateAccessTokenUseCase,
                    ReadRaffleUseCase readRaffleUseCase,
                    RaffleAPrizeUseCase raffleAPrizeUseCase,
                    CommitAPrizeUseCase commitAPrizeUseCase) {
        this.cookieName = cookieName
        this.validateAccessTokenUseCase = validateAccessTokenUseCase
        this.readRaffleUseCase = readRaffleUseCase
        this.raffleAPrizeUseCase = raffleAPrizeUseCase
        this.commitAPrizeUseCase = commitAPrizeUseCase
    }

    @Get("/")
    @Secured(SecurityRule.IS_ANONYMOUS)
    @View("index")
    HttpResponse<Map> index(HttpRequest<?> request, @Nullable Authentication auth) {
        if( !accessToken(request, auth))
            return HttpResponse.ok([:])

        return HttpResponse.ok([userName:auth.attributes['name']])
    }

    @Get("/current")
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @View("raffle")
    @TurboFrameView("raffle")
    @Produces(MediaType.TEXT_HTML)
    HttpResponse<Map> current(HttpRequest<?> request, @Nullable Authentication auth, String sheetId, String tabId) {
        String accessToken = accessToken(request, auth)
        if( !accessToken )
            HttpResponse.badRequest()

        Raffle raffle = readRaffleUseCase.execute(accessToken, sheetId, tabId)
        if( !raffle )
            HttpResponse.badRequest()

        HttpResponse.ok([
                participants:raffle.shuffleParticipants,
                prizes: raffle.prizes,
                completed: raffle.completed,
                sheetId: sheetId,
                tabId: tabId
        ])
    }

    @Get("/raffle")
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @View("winner")
    @TurboFrameView("winner")
    @Produces(MediaType.TEXT_HTML)
    HttpResponse<Map> raffle(HttpRequest<?> request, @Nullable Authentication auth, String sheetId, String tabId, String winner) {
        String accessToken = accessToken(request, auth)
        if( !accessToken )
            HttpResponse.badRequest()

        def turn = raffleAPrizeUseCase.execute(accessToken, sheetId, tabId, winner)

        HttpResponse.ok([
                winner: turn?.participant,
                prize: turn?.prize,
                participants: turn?.raffle?.participants,
                completed: turn ? turn.raffle.completed : true,
                prizes: turn?.raffle?.prizes,
                sheetId: sheetId,
                tabId: tabId
        ])
    }

    @Get("/commit")
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @View("raffle")
    @TurboFrameView("raffle")
    @Produces(MediaType.TEXT_HTML)
    HttpResponse<Map> commit(HttpRequest<?> request, @Nullable Authentication auth, String sheetId, String tabId, String winner, String prize, String answer) {
        String accessToken = accessToken(request, auth)
        if( !accessToken )
            HttpResponse.badRequest()

        def raffle = commitAPrizeUseCase.execute(accessToken, sheetId, tabId, winner, prize, answer)

        HttpResponse.ok([
                participants:raffle.shuffleParticipants,
                prizes: raffle.prizes,
                completed: raffle.completed,
                sheetId: sheetId,
                tabId: tabId
        ])
    }

    private String accessToken(HttpRequest<?> request, @Nullable Authentication auth){
        if( ! auth ) {
            log.info "No auth for request $request.uri"
            return null
        }
        def cookie = request.cookies.all.find { it.name == cookieName }
        if( !cookie ) {
            log.info "No cookie for request $request.uri"
            return null
        }
        if( !validateAccessTokenUseCase.execute(cookie.value) ) {
            log.info "No valid access token for request $request.uri"
            return null
        }
        cookie.value
    }
}
