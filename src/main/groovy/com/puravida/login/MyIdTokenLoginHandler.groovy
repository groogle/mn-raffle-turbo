package com.puravida.login

import io.micronaut.context.annotation.Primary
import io.micronaut.context.annotation.Replaces
import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpRequest
import io.micronaut.http.cookie.Cookie
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.config.RedirectConfiguration
import io.micronaut.security.config.SecurityConfigurationProperties
import io.micronaut.security.errors.PriorToLoginPersistence
import io.micronaut.security.oauth2.endpoint.token.response.IdTokenLoginHandler
import io.micronaut.security.token.jwt.cookie.AccessTokenCookieConfiguration
import jakarta.inject.Singleton
import io.micronaut.core.annotation.Nullable

import java.time.Duration;

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Requires(property = "micronaut.security.authentication", value = "idtoken")
@Singleton
@Primary
class MyIdTokenLoginHandler extends IdTokenLoginHandler {

    String cookieName

    MyIdTokenLoginHandler(AccessTokenCookieConfiguration accessTokenCookieConfiguration,
                          RedirectConfiguration redirectConfiguration,
                          @Value('${raffle.cookie}') String cookieName,
                          @Nullable PriorToLoginPersistence priorToLoginPersistence) {
        super(accessTokenCookieConfiguration, redirectConfiguration, priorToLoginPersistence)
        this.cookieName = cookieName
    }

    @Override
    List<Cookie> getCookies(Authentication authentication, HttpRequest<?> request) {
        List<Cookie> ret = super.getCookies(authentication, request)
        String accessToken = authentication.attributes['accessToken']
        Cookie jwtCookie = Cookie.of(cookieName, accessToken)
        jwtCookie.configure(accessTokenCookieConfiguration, request.isSecure())
        jwtCookie.maxAge(Duration.ofDays(1))
        ret.add(jwtCookie)
        return ret
    }
}
