package com.puravida.model


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
class Participant {
    String name
    String email
    String prize

    void addPrize(Prize add){
        prize = ((prize?:"").split(",")+[add.name]).join(',')
    }

    void removePrize(Prize remove){
        def prizes = (prize?:"").split(",")
        prize = prizes.findAll{ it != remove.name}.join(',')
    }
}
