package com.puravida.model

import groovy.transform.CompileStatic


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@CompileStatic
class Raffle {

    List<Participant> participants = []
    List<Prize> prizes = []


    static Raffle fromList(List<List<Object>>data){
        Raffle ret = new Raffle()
        data.each{row ->
            if( row[0] ){
                ret.prizes << new Prize(name: row[0].toString(), quantity: row[1] ? row[1] as int : null)
            }
            if( row[2] ){
                def p = new Participant(name: row[2].toString(), prize: row[3]?.toString(), email: row[4]?.toString())
                ret.participants << p
            }
        }
        ret
    }

    List<List<Object>>toList(){
        int size = Math.max(participants.size(), prizes.size())
        List<List<Object>> data = []
        (0..size-1).each{idx->
            def row = []
            row[0] = prizes[idx]?.name ?: ""
            row[1] = prizes[idx]?.quantity ?: 0
            row[2] = participants[idx]?.name ?: ""
            row[3] = participants[idx]?.prize ?: ""
            row[4] = participants[idx]?.email ?: ""
            data << row
        }
        data
    }

    List<Participant> getShuffleParticipants(){
        def clone = new ArrayList(participants.findAll{
            !it.name.startsWith('--') && !it.prize
        })
        clone.shuffle(new Random())
        clone
    }

    Random r = new Random()

    Prize pickARandomPrize(){
        def availables = prizes.findAll{ it.quantity}
        if( !availables )
            return null
        def selected = availables.get(r.nextInt(availables.size()))
        selected.quantity--
        selected
    }

    Participant pickARandomParticipant(){
        def list = shuffleParticipants
        def selected = list.get(r.nextInt(list.size()))
        selected
    }

    Turn nextRaffle(String winner){
        def prize = prizes.find{ it.quantity}
        if( !prize )
            return null
        def participant = participants.find{ it.name == winner}
        new Turn(prize: prize, participant: participant, raffle: this)
    }

    Turn randomRaffle(String winner){
        def prize = pickARandomPrize()
        if( !prize )
            return null
        def participant = participants.find{ it.name == winner}
        new Turn(prize: prize, participant: participant, raffle: this)
    }

    boolean isCompleted(){
        prizes.find{ it.quantity } == null
    }

}
