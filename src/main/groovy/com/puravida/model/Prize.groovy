package com.puravida.model


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
class Prize {

    String name
    Integer quantity

}
