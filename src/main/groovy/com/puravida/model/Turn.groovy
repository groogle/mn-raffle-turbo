package com.puravida.model


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
class Turn {

    Prize prize
    Participant participant
    Raffle raffle

}
