package com.puravida.usecases

import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.SheetService
import com.puravida.groogle.SheetServiceBuilder
import groovy.transform.PackageScope
import io.micronaut.context.annotation.Value
import jakarta.inject.Singleton


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Singleton
@PackageScope
class GroogleInstance {

    @Value('${groogle.client}')
    String clientCredentialsFile

    @Value('${groogle.service}')
    String serviceCredentialsFile

    Groogle serviceInstance(){
        def file = serviceCredentialsFile
        GroogleBuilder.build {
            withServiceCredentials {
                scopes SheetsScopes.SPREADSHEETS
                usingCredentials file
            }
            register SheetServiceBuilder.build(), SheetService
        }
    }

    Groogle instance(String theAccessToken){
        def file = clientCredentialsFile
        GroogleBuilder.build {
            withAccessToken {
                applicationName 'mn-raffle'
                usingCredentials file
                accessToken theAccessToken
            }
            register SheetServiceBuilder.build(), SheetService
        }.with {groogle ->
            assert service(SheetService) != null, "No SheetService available"
            groogle
        }
    }

}

