package com.puravida.usecases

import com.puravida.groogle.SheetService
import com.puravida.model.Participant
import com.puravida.model.Prize
import com.puravida.model.Raffle
import jakarta.inject.Singleton


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Singleton
class ReadRaffleUseCase {

    GroogleInstance groogleInstance

    ReadRaffleUseCase(GroogleInstance groogleInstance) {
        this.groogleInstance = groogleInstance
    }

    Raffle execute(String theAccessToken , String sheetId, String tabId){
        groogleInstance.serviceInstance().with{
            Raffle ret

            service SheetService with {
                withSpreadSheet sheetId, {
                    withSheet tabId,{
                        def range = writeRange "A2", "E100"
                        def data = range.get()
                        ret = Raffle.fromList(data)
                    }
                }
            }

            return ret
        }
    }

}
