package com.puravida.usecases

import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.SheetService
import com.puravida.groogle.SheetServiceBuilder
import groovy.util.logging.Slf4j
import io.micronaut.context.annotation.Value
import jakarta.inject.Singleton


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Slf4j
@Singleton
class ValidateAccessTokenUseCase {

    GroogleInstance groogleInstance

    ValidateAccessTokenUseCase(GroogleInstance groogleInstance) {
        this.groogleInstance = groogleInstance
    }

    boolean execute(String theAccessToken ){
        try{
            assert groogleInstance.serviceInstance() != null
            return true
        }catch( Exception e){
            log.error "Validating accesstoken $theAccessToken", e
            return false
        }
    }

}
