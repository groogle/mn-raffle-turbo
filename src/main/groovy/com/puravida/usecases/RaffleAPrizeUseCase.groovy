package com.puravida.usecases

import com.puravida.groogle.SheetService
import com.puravida.model.Participant
import com.puravida.model.Prize
import com.puravida.model.Raffle
import com.puravida.model.Turn
import jakarta.inject.Singleton


/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Singleton
class RaffleAPrizeUseCase {

    GroogleInstance groogleInstance

    RaffleAPrizeUseCase(GroogleInstance groogleInstance) {
        this.groogleInstance = groogleInstance
    }

    Turn execute(String theAccessToken, String sheetId, String tabId, String winner){
        groogleInstance.serviceInstance().with{
            def turn
            service SheetService with {
                withSpreadSheet sheetId, {
                    withSheet tabId,{
                        def range = writeRange "A2", "E100"
                        def data = range.get()
                        def raffle = Raffle.fromList(data)

                        turn = raffle.nextRaffle(winner)
                        if( turn ) {
                            turn.participant.addPrize(turn.prize)
                        }
                    }
                }
            }
            turn
        }
    }

}
