package com.puravida.usecases

import com.puravida.groogle.SheetService
import com.puravida.model.Raffle
import com.puravida.model.Turn
import jakarta.inject.Singleton

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Singleton
class CommitAPrizeUseCase {

    GroogleInstance groogleInstance

    CommitAPrizeUseCase(GroogleInstance groogleInstance) {
        this.groogleInstance = groogleInstance
    }

    Raffle execute(String theAccessToken, String sheetId, String tabId, String winnerName, String prizeName, String action){
        groogleInstance.serviceInstance().with{
            def raffle
            service SheetService with {
                withSpreadSheet sheetId, {
                    withSheet tabId,{
                        def range = writeRange "A2", "E100"
                        def data = range.get()
                        raffle = Raffle.fromList(data)

                        def participant = raffle.participants.find{ it.name == winnerName }
                        def prize = raffle.prizes.find{ it.name == prizeName }
                        assert participant, "Participant $winnerName not found"
                        assert prize, "Prize $prizeName not found"

                        switch (action){
                            case 'yes':
                                //nothing to do
                                prize.quantity--
                                break
                            case 'no':
                                participant.removePrize(prize)
                                break
                            case 'absent':
                                participant.name='--'+participant.name
                                participant.removePrize(prize)
                                break
                        }
                        range.set raffle.toList()
                    }
                }
            }
            raffle
        }
    }

}
