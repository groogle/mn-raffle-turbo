
function initParticipants(theList, callback) {
    var participants = []
    for (i = 0; i < theList.length; i++) {
        participants.push({
            fillStyle: "#"+Math.floor(Math.random()*16777215).toString(16),
            text: theList[i].name
        });
    }
    var duration = 3+Math.floor(Math.random() * 3);
    window.miRuleta = new Winwheel({
        numSegments: participants.length,
        outerRadius: 170,
        segments: participants,
        animation: {
            type: "spinToStop",
            duration: duration,
            callbackFinished: "window.miRuleta.callbackFinished()",
            callbackAfter: "window.miRuleta.dibujarIndicador()"
        }
    })

    window.miRuleta.callbackFinished = function(){
        var winner = window.miRuleta.getIndicatedSegment();
        window.miRuleta.stopAnimation(false);
        callback(winner.text)
    }

    window.miRuleta.dibujarIndicador = function () {
        var ctx = window.miRuleta.ctx;
        ctx.strokeStyle = "navy";
        ctx.fillStyle = "black";
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(170, 10);
        ctx.lineTo(230, 10);
        ctx.lineTo(200, 70);
        ctx.lineTo(170, 10);
        ctx.stroke();
        ctx.fill();
    }
    window.miRuleta.dibujarIndicador()

}